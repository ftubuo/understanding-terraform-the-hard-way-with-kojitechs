
locals {
  azs  = data.aws_availability_zones.available.names
}

################################################################################
#  CREATING VPC
################################################################################
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

################################################################################
# USING COUNT TO CREATE PUBLIC SUBNETS
################################################################################
resource "aws_subnet" "public_subnet" {
    count = 2

  vpc_id     = aws_vpc.this.id
  cidr_block = var.public_cidr[count.index] 
  availability_zone = [local.azs[0], local.azs[1]][count.index] 
  map_public_ip_on_launch = true # ALWAYS ASSIGN AN IP | 

  tags = {
    Name = "public_subnet_${count.index + 1}" 
  }
}

################################################################################
# USING COUNT TO CREATE PRIVATE SUBNETS
################################################################################

resource "aws_subnet" "private_subnet" {
  count = 2

  vpc_id     = aws_vpc.this.id 
  cidr_block = var.private_cidrs[count.index]
  availability_zone = slice(local.azs,0,2)[count.index] #[local.azs[0], local.azs[1]][count.index]
  
  tags = {
    Name = "private_subnet_${count.index +1}" 
  }
}



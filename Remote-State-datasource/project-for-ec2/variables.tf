

################################################################################
# ROOT MODULE
################################################################################

variable "component" {
  type        = string
  description = "Name of the project we are working on"
  default     = "project-for-ec2"
}

################################################################################
# CONFIGURE BACKEND
################################################################################

terraform {
  required_version = ">=1.1.0"

  backend "s3" {
    bucket         = "project-for-ec2"
    key            = "path/env"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
################################################################################
# USING REMOTE STATE DATASOURCE TO FETCH RESOURCE ATTRIBUTE FROM PROJECT-FOR-VPC
################################################################################

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "project-for-vpc"
    key =  format("env:/%s/path/env", terraform.workspace) # "env:/${terraform.workspace}/path/env"
    region = "us-east-1"
  }
}

locals {
  operation_vpc = data.terraform_remote_state.vpc.outputs
  vpc_id = local.operation_vpc.vpc_id
  public_subnets = local.operation_vpc.public_subnets
}

## ANOTHER ACCOUNT
# data "terraform_remote_state" "vpc" {
#   backend = "s3"

#   config {
#     bucket = "project-for-vpc"
#     key =  "env:/sbx/path/env" # statefile
#     region = "us-east-1"
#   }
# }


################################################################################
# CREATING AN EC2 INSTANCE USING COUNT 
################################################################################

data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}


resource "aws_iam_role" "ssm_fleet_instance" {
  name = "${var.component}-ssm-234-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
  tags = {
    Name = "${var.component}-ssm-234-role"
  }
}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "${var.component}-ssm-fleet23d-instance-profile"
  role = aws_iam_role.ssm_fleet_instance.name
}

resource "aws_iam_policy" "policy" {
  name        = "${var.component}-ssm-ew3-policy"
  description = "allow ecs instance to be managed by ssm"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
         "ssm:UpdateInstanceInformation",
          "ssmmessages:CreateControlChannel",
          "ssmmessages:CreateDataChannel",
          "ssmmessages:OpenControlChannel",
          "ssmmessages:OpenDataChannel"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }   
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ec2_policy_attach" {
  role       = aws_iam_role.ssm_fleet_instance.name
  policy_arn = aws_iam_policy.policy.arn
}

################################################################################
# CREATING AN EC2 INSTANCE USING COUNT 
################################################################################

resource "aws_instance" "app1" {
  ami                    = data.aws_ami.ami.id
  instance_type          = "t2.micro"
  subnet_id              = local.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.app_static_sg.id]
  iam_instance_profile   = aws_iam_instance_profile.instance_profile.name
  user_data              = file("${path.module}/templates/app1.sh")
  tags = {
    Name = "app1"
  }
}
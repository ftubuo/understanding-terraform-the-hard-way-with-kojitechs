

output "bucket_names" {
    value = aws_s3_bucket.this[*].bucket
}

output "dynamodb_name" {
    value = aws_dynamodb_table.dynamodb-terraform-lock.name
}

## TYPES( ROOT/ CHILD)
# CLASSES OF MODULES( public / private modules)

# for specific company( store github: private... module())

## PUBLIC (VPC MODULE: )

module "vpc" {
  source = "terraform-aws-modules/vpc/aws" 

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs                = slice(data.aws_availability_zones.azs.names, 0,3)  # 3 
  private_subnets    = var.private_subnets_cidr
  public_subnets     = var.public_subnets_cidr
  enable_nat_gateway = false
  enable_vpn_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

# # ### 
# # we have to get vpc id 
# # ""  ""     ""   subnet id(public subnet id)

# # ### 
# ####
# # how to get the vpc id that was created from the module.
# #  output "name" {
# #     value = ""
# #  }



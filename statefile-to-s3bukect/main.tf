

data "aws_availability_zones" "available" {
  state = "available"
}

################################################################################
# USING COUNT TO CREATE PUBLIC SUBNETS
################################################################################
locals {
  azs  = data.aws_availability_zones.available.names

  public_subnet = {
    public_subnet_1 = {
        cidr_block = "10.0.0.0/24"
        availability_zone = local.azs[0]
    }
  }

    private_subnet = { 
      kojitechs_private_sub1 ={
        cidr_block = "10.0.1.0/24"
        availability_zone = local.azs[0]
      }
      kojitechs_private_sub2 ={
        cidr_block = "10.0.2.0/24"
        availability_zone = local.azs[1]
      }
      
    }
}
################################################################################
# CREATING VPC
################################################################################

resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

################################################################################
# USING FOR_EACH TO CREATE PUBLIC SUBNETS
################################################################################
resource "aws_subnet" "public_subnet" {
    for_each = local.public_subnet 

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block
  availability_zone = each.value.availability_zone
  map_public_ip_on_launch = true 

  tags = {
    Name = each.key
  }
}

################################################################################
# EC2
################################################################################
resource "aws_instance" "web" {
  ami           = var.ami_id # " "
  instance_type = "t3.micro" # ""
  subnet_id = aws_subnet.public_subnet["public_subnet_1"].id

  tags = {
    Name = "HelloWorld"
  }
}

variable "ami_id" {
  type        = string
  description = "ami id"
  default     = "ami-09d3b3274b6c5d4aa"
}


################################################################################
# USING FOR_EACH TO CREATE PRIVATE SUBNETS
################################################################################
resource "aws_subnet" "private_subnet" {
  for_each = local.private_subnet

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value["cidr_block"]
  availability_zone = each.value["availability_zone"]

  tags = {
    Name = each.key
  }
}

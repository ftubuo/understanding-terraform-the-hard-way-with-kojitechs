
################################################################################
# CREATING AN EC2 INSTANCE USING COUNT 
################################################################################

data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

################################################################################
# CREATING AN EC2 INSTANCE USING COUNT 
################################################################################

resource "aws_instance" "jenkins-server" {
  ami                    = data.aws_ami.ami.id
  instance_type          = var.instance_type
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.jenkins_sg.id]
  iam_instance_profile   = aws_iam_instance_profile.instance_profile.name
  user_data              = file("${path.module}/templates/jenkins.sh")
  tags = {
    Name = "jenkins-server"
  }
}

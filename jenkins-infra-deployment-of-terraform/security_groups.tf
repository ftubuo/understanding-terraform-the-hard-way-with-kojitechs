################################################################################
# CREATING ALB SECUIRTY  GROUP
################################################################################

resource "aws_security_group" "jenkins_sg" {
  name        = "${var.component}-jenkins-sg"
  description = "Allow access jenkins ingress access"
  vpc_id      = local.vpc_id

  tags = {
    Name = "${var.component}-jenkins-sg"
  }
}

resource "aws_security_group_rule" "ingress_access_on_http" {

  security_group_id = aws_security_group.jenkins_sg.id
  type              = "ingress"
  from_port         = 8080
  to_port           = 8080
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "egress_access_from_everywhere" {
  security_group_id = aws_security_group.jenkins_sg.id
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  from_port         = 0
  cidr_blocks       = ["0.0.0.0/0"]
}
